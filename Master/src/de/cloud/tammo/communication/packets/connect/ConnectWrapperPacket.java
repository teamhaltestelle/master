package de.cloud.tammo.communication.packets.connect;

import de.cloud.tammo.communication.Packet;

/**
 * Created by Tammo on 27.06.2017.
 */
public class ConnectWrapperPacket extends Packet{

    private boolean later;

    public ConnectWrapperPacket(boolean later) {
        super("CONNECTWRAPPER");
        this.later = later;
    }

    public boolean isLater() {
        return later;
    }
}
