package de.cloud.tammo.communication.packets.connect;

import de.cloud.tammo.communication.Packet;

/**
 * Created by Tammo on 27.06.2017.
 */
public class DisconnectWrapperPacket extends Packet{

    public DisconnectWrapperPacket() {
        super("DISCONNECTWRAPPER");
    }
}
