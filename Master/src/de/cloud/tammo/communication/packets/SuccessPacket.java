package de.cloud.tammo.communication.packets;

import de.cloud.tammo.communication.Packet;

/**
 * Created by Tammo on 29.06.2017.
 * Copyright by Tammo
 */
public class SuccessPacket extends Packet{

    public SuccessPacket() {
        super("SUCCESS");
    }
}
