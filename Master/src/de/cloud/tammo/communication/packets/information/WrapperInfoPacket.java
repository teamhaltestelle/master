package de.cloud.tammo.communication.packets.information;

import de.cloud.tammo.communication.Packet;

/**
 * Created by Tammo on 30.06.2017.
 * Copyright by Tammo
 */
public class WrapperInfoPacket extends Packet{

    private long ram;
    private int cpu;

    public WrapperInfoPacket(long ramauslastung, int cpupercent) {
        super("WRAPPERINFO");
        this.ram = ramauslastung;
        this.cpu = cpupercent;
    }

    public int getCpu() {
        return cpu;
    }

    public long getRam() {
        return ram;
    }
}
