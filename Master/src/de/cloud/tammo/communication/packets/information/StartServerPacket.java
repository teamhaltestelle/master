package de.cloud.tammo.communication.packets.information;

import de.cloud.tammo.communication.Packet;

/**
 * Created by Tammo on 28.06.2017.
 */
public class StartServerPacket extends Packet{

    private String servername, template;
    private int port, minRAM, maxRAM;

    public StartServerPacket(String servername, int port, int minRAM, int maxRAM, String template) {
        super("SERVERSTART");
        this.servername = servername;
        this.port = port;
        this.minRAM = minRAM;
        this.maxRAM = maxRAM;
        this.template = template;
    }

    public int getPort() {
        return port;
    }

    public String getServerName() {
        return servername;
    }

    public int getMinRAM() {
        return minRAM;
    }

    public int getMaxRAM() {
        return maxRAM;
    }

    public String getTemplate() {
        return template;
    }
}
