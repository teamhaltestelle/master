package de.cloud.tammo.communication.packets.information;

import de.cloud.tammo.communication.Packet;

/**
 * Created by Tammo on 30.06.2017.
 * Copyright by Tammo
 */
public class ServerErrorPacket extends Packet{

    private String Error;

    public ServerErrorPacket(String Error) {
        super("SERVERERROR");
        this.Error = Error;
    }

    public String getError() {
        return Error;
    }
    
}
