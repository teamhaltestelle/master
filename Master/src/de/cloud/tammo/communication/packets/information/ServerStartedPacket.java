package de.cloud.tammo.communication.packets.information;

import de.cloud.tammo.communication.Packet;

/**
 * Created by Tammo on 30.06.2017.
 * Copyright by Tammo
 */
public class ServerStartedPacket extends Packet{

    public ServerStartedPacket() {
        super("SERVERSTARTED");
    }
}
