package de.cloud.tammo.communication;

import de.cloud.tammo.master.Master;
import de.cloud.tammo.master.logger.Logger;
import de.cloud.tammo.master.logger.MessageType;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Tammo on 30.06.2017.
 * Copyright by Tammo
 */
public class PacketServer {

    private int port;
    private boolean run;
    private ServerSocket serverSocket;
    private ExecutorService executorService;
    private HashMap<String, Handler> handler;

    public PacketServer(int port) {
        this.port = port;
        this.executorService = Executors.newCachedThreadPool();
        this.handler = new HashMap<>();
        this.run = false;
    }

    public void bind(){
        close();
        new Thread(() -> {
            try {
                serverSocket = new ServerSocket(port);
                run = true;
                while (run){
                    Socket socket = serverSocket.accept();
                    ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
                    try {
                        Object object = in.readObject();
                        boolean isPacket = object instanceof Packet;
                        String ip = socket.getInetAddress().getHostAddress();
                        if(!Master.getIpConfig().getIps().contains(ip) && !isPacket){
                            socket.close();
                            Logger.addMessage("Unerlaubter Zugriff!", MessageType.WARNING);
                            return;
                        }
                        Packet packet = (Packet) object;
                        if(this.handler.containsKey(packet.getName())){
                            this.handler.get(packet.getName()).handle(packet, socket);
                        }else{
                            Logger.addMessage("Fehler beim verarbeiten!", MessageType.ERROR);
                        }
                        in.close();
                        socket.close();
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    public void close(){
        run = false;
        if(serverSocket != null){
            try {
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        serverSocket = null;
    }

    public void sendPacket(Packet packet, String host, int port){
        try {
            Socket socket = new Socket(host, port);
            ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
            out.writeObject(packet);
            out.flush();
            out.close();
            socket.close();
        } catch (IOException e) {
            Logger.addMessage("Konnte keine Verbindung aufbauen!", MessageType.ERROR);
        }
    }

    public void addHandler(String handle, Handler handler){
        if(!this.handler.containsKey(handle)){
            this.handler.put(handle, handler);
        }
    }
}
