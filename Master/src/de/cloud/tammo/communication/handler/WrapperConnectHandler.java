package de.cloud.tammo.communication.handler;

import de.cloud.tammo.communication.Handler;
import de.cloud.tammo.communication.Packet;
import de.cloud.tammo.communication.packets.connect.ConnectWrapperPacket;
import de.cloud.tammo.communication.packets.SuccessPacket;
import de.cloud.tammo.communication.packets.connect.WrapperConnectPacket;
import de.cloud.tammo.master.Master;
import de.cloud.tammo.master.logger.Logger;
import de.cloud.tammo.master.wrapper.Wrapper;

import java.net.Socket;

/**
 * Created by Tammo on 27.06.2017.
 */
public class WrapperConnectHandler extends Handler{

    @Override
    public Packet handle(Packet p, Socket socket) {
        WrapperConnectPacket packet = (WrapperConnectPacket) p;
        Wrapper w = Master.getWrapperHandler().getWrapperByHost(socket.getInetAddress().getHostAddress());
        w.setConnected(true);
        Logger.addMessage("Wrapper-" + w.getId() + " ist nun verbunden!");
        w.save();
        if(packet.isLater()){
           Master.getServer().sendPacket(new ConnectWrapperPacket(true), socket.getInetAddress().getHostAddress(), 226);
        }
        return new SuccessPacket();
    }
}
