package de.cloud.tammo.communication.handler;

import de.cloud.tammo.communication.Handler;
import de.cloud.tammo.communication.Packet;
import de.cloud.tammo.communication.packets.SuccessPacket;
import de.cloud.tammo.communication.packets.information.WrapperInfoPacket;
import de.cloud.tammo.master.Master;
import de.cloud.tammo.master.wrapper.Wrapper;

import java.net.Socket;

/**
 * Created by Tammo on 30.06.2017.
 * Copyright by Tammo
 */
public class WrapperInfoHandler extends Handler{

    public Packet handle(Packet p, Socket socket) {
        WrapperInfoPacket packet = (WrapperInfoPacket) p;
        Wrapper w = Master.getWrapperHandler().getWrapperByHost(socket.getInetAddress().getHostAddress());
        w.setRam(packet.getRam());
        w.setCpu(packet.getCpu());
        return new SuccessPacket();
    }
}
