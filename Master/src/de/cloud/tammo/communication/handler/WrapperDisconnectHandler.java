package de.cloud.tammo.communication.handler;

import de.cloud.tammo.communication.Handler;
import de.cloud.tammo.communication.Packet;
import de.cloud.tammo.communication.packets.SuccessPacket;
import de.cloud.tammo.communication.packets.connect.WrapperDisconnectPacket;
import de.cloud.tammo.master.Master;
import de.cloud.tammo.master.logger.Logger;
import de.cloud.tammo.master.wrapper.Wrapper;

import java.net.Socket;

/**
 * Created by Tammo on 27.06.2017.
 */
public class WrapperDisconnectHandler extends Handler{

    @Override
    public Packet handle(Packet p, Socket socket) {
        WrapperDisconnectPacket packet = (WrapperDisconnectPacket) p;
        Wrapper w = Master.getWrapperHandler().getWrapperByHost(socket.getInetAddress().getHostAddress());
        w.setConnected(false);
        Logger.addMessage("Wrapper-" + w.getId() + " hat sich verabschiedet...");
        w.save();
        return new SuccessPacket();
    }
}
