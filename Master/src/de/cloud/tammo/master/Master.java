package de.cloud.tammo.master;

import de.cloud.tammo.communication.PacketServer;
import de.cloud.tammo.communication.handler.ErrorPacketHandler;
import de.cloud.tammo.communication.handler.WrapperConnectHandler;
import de.cloud.tammo.communication.handler.WrapperDisconnectHandler;
import de.cloud.tammo.communication.handler.WrapperInfoHandler;
import de.cloud.tammo.communication.packets.connect.ConnectWrapperPacket;
import de.cloud.tammo.communication.packets.connect.DisconnectWrapperPacket;
import de.cloud.tammo.master.cloud.Cloud;
import de.cloud.tammo.master.command.Command;
import de.cloud.tammo.master.command.CommandManager;
import de.cloud.tammo.master.config.IPConfig;
import de.cloud.tammo.master.config.UserConfig;
import de.cloud.tammo.master.config.UserHandler;
import de.cloud.tammo.master.groups.GroupManager;
import de.cloud.tammo.master.logger.Logger;
import de.cloud.tammo.master.logger.MessageType;
import de.cloud.tammo.master.wrapper.Wrapper;
import de.cloud.tammo.master.wrapper.WrapperHandler;

import java.util.Scanner;

import static de.cloud.tammo.master.logger.MessageType.ERROR;


/**
 * Created by Tammo on 26.06.2017.
 * Copyright by Tammo
 */
public class Master {

    private static PacketServer server;
    private static UserHandler userHandler;
    private static UserConfig userConfig;
    private static WrapperHandler wrapperHandler;
    private static GroupManager groupManager;
    private static IPConfig ipConfig;
    private static Cloud cloud;

    public static void main(String[] args){
        new Master();
    }

    public Master() {

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            Shutdown();
        }));

        Logger.addMessage("Starte Master...");

        try{

            Thread.sleep(100);

            Logger.addMessage(" _____     ____   _      _____   _   _   _____  "); Thread.sleep(50);
            Logger.addMessage("|__ __|   |  __| | |    | ___ | | | | | |  __ | "); Thread.sleep(50);
            Logger.addMessage("  | |     | |    | |    ||   || | | | | | |  | |"); Thread.sleep(50);
            Logger.addMessage("  | |     | |__  | |__  ||___|| | |_| | | |__| |"); Thread.sleep(50);
            Logger.addMessage("  |_|     |____| |____| |_____| |_____| |_____| "); Thread.sleep(50);

            Logger.addMessage(" "); Thread.sleep(100);
            Logger.addMessage("Copyright by Tammo [Haltestelle]"); Thread.sleep(100);
            Logger.addMessage(" "); Thread.sleep(100);

        }catch (InterruptedException e){
            Logger.addMessage("ERROR", ERROR);
        }

        setupPacketServer();

        new CommandManager();

        Scanner sc = new Scanner(System.in);
        String line;

        userHandler = new UserHandler();
        userConfig = new UserConfig();

        ipConfig = new IPConfig();

        if(!userHandler.isLogggedIn()){
            Logger.addMessage("Logge dich nun ein!");
            login(sc);
        }

        wrapperHandler = new WrapperHandler();

        groupManager = new GroupManager();

        if(wrapperHandler.getWrappers().isEmpty()){
            setup(sc);
        }else{
            connectToWrappers();
        }

        cloud = new Cloud();

        while ((line = sc.nextLine()) != null){
            boolean wasfound = false;
            final String[] arguments = line.split(" ");
            final String cmd = arguments[0];
            for(Command c : CommandManager.getCommands()){
                if (c.getCommand().equalsIgnoreCase(cmd)) {
                    wasfound = true;
                    final String[] arg = new String[arguments.length - 1];
                    System.arraycopy(arguments, 1, arg, 0, arg.length);
                    if(arg.length != 0){
                        if(arg[0].equalsIgnoreCase("help")){
                            c.sendhelp();
                        }
                    }
                    c.execute(arg);
                }
                if(c.getAliases().length != 0){
                    for(String alias : c.getAliases()){
                        if (alias.equalsIgnoreCase(cmd)) {
                            wasfound = true;
                            final String[] arg = new String[arguments.length - 1];
                            System.arraycopy(arguments, 1, arg, 0, arg.length);
                            if(arg.length != 0){
                                if(arg[0].equalsIgnoreCase("help")){
                                    c.sendhelp();
                                }
                            }
                            c.execute(arg);
                        }
                    }
                }
            }
            if(!wasfound){
                Logger.addMessage("Diesen Command gibt es nicht!", ERROR);
            }
        }

    }

    private void login(Scanner sc){
        Logger.addMessage("Bitte gebe deinen Username ein: ");
        String name = sc.nextLine();
        if(userHandler.usernameExist(name)){
            Logger.addMessage("Bitte gebe dein Passwort ein: ");
            String password = sc.nextLine();
            if(!userHandler.checkAuth(name, password)){
                login(sc);
            }
        }else{
            Logger.addMessage("Dieser Username existiert nicht!", ERROR);
            login(sc);
        }
    }

    private void setup(Scanner sc){
        Logger.addMessage("");
        Logger.addMessage("Herzlich Willkommen zum Setup der T Cloud!");
        Logger.addMessage("");
        Logger.addMessage("Bitte trage die IP von Wrapper-1 ein!");
        String ip = sc.nextLine();
        Wrapper w = new Wrapper(ip, wrapperHandler.getNextID());
        Logger.addMessage("Es wird nun nach Wrapper-" + w.getId() + " gesucht!");
        wrapperHandler.addWrapper(w);
        wrapperHandler.searchforWrapper(ip);
    }

    private void connectToWrappers(){
        int connected = 0;
        Logger.addMessage("");
        for(Wrapper w : wrapperHandler.getWrappers()){
            if(!w.isConnected()){
                server.sendPacket(new ConnectWrapperPacket(false), w.getAdress(), 226);
                connected++;
                Logger.addMessage("Suche nach Wrapper-" + w.getId() + "...");
            }
        }
        if(connected == 0){
            Logger.addMessage("Es ist kein verfügbarer Wrapper gestartet!", MessageType.WARNING);
            Logger.addMessage("Starte einen Wrapper oder füge einen hinzu! [Wrapper add]", MessageType.WARNING);
        }
    }

    private void setupPacketServer(){
        server = new PacketServer(225);
        server.bind();
        server.addHandler("ERROR", new ErrorPacketHandler());
        server.addHandler("WRAPPERCONNECT", new WrapperConnectHandler());
        server.addHandler("WRAPPERDISCONNECT", new WrapperDisconnectHandler());
        server.addHandler("WRAPPERINFO", new WrapperInfoHandler());
    }

    private void Shutdown(){
        Logger.addMessage("Master stoppt...");
        for(Wrapper w : wrapperHandler.getWrappers()){
            if(w.isConnected()){
                server.sendPacket(new DisconnectWrapperPacket(), w.getAdress(), 226);
            }
        }
        userConfig.save();
        server.close();
    }

    public static PacketServer getServer() {
        return server;
    }

    public static UserHandler getUserHandler() {
        return userHandler;
    }

    public static UserConfig getUserConfig() {
        return userConfig;
    }

    public static WrapperHandler getWrapperHandler() {
        return wrapperHandler;
    }

    public static GroupManager getGroupManager() {
        return groupManager;
    }

    public static IPConfig getIpConfig() {
        return ipConfig;
    }
}