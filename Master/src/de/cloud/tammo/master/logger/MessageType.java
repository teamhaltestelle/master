package de.cloud.tammo.master.logger;

/**
 * Created by Tammo on 27.06.2017.
 */
public enum MessageType {

    NORMAL,
    WARNING,
    ERROR;

}
