package de.cloud.tammo.master.cloud;

import de.cloud.tammo.communication.packets.information.StartServerPacket;
import de.cloud.tammo.master.Master;
import de.cloud.tammo.master.groups.Group;
import de.cloud.tammo.master.groups.GroupType;
import de.cloud.tammo.master.logger.Logger;
import de.cloud.tammo.master.logger.MessageType;
import de.cloud.tammo.master.wrapper.Wrapper;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Tammo on 29.06.2017.
 * Copyright by Tammo
 */
public class Cloud {

    private ExecutorService executorService;
    private Thread t;
    private boolean run;

    public Cloud(){
        executorService = Executors.newCachedThreadPool();
        run = false;
        start();
    }

    public void start(){
        executorService.submit(() -> {
            t = new Thread(new Runnable() {
                @Override
                public void run() {
                    run = true;
                    while(run){
                        check();
                        try {
                            t.sleep(2500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            t.start();
        });
    }

    public void stop(){
        run = false;
    }

    private void check(){
        if(!multiroot()){
            Wrapper w = Master.getWrapperHandler().getWrapperByID(1);
            for(Group g : Master.getGroupManager().getGroups()){
                if(g.getType() == GroupType.DYNAMISCH){
                    if(g.getAmount() < g.getMax()){
                        Master.getServer().sendPacket(new StartServerPacket(g.getName() + " ", g.getStartPort(), g.getMinRAM(), g.getMaxRAM(), g.getName()), w.getAdress(), 226);
                    }
                }else{

                }
            }
        }else{
            Logger.addMessage("Keine MultiRoot Unterstützung!", MessageType.ERROR);
        }
    }

    private long getRam(int ram){
        return ram * 1024 * 1024;
    }

    private boolean multiroot(){
        int i = 0;
        for(Wrapper w : Master.getWrapperHandler().getWrappers()){
            if(w.isConnected()){
                i++;
            }
        }
        if(i > 1){
            return true;
        }
        return false;
    }

}
