package de.cloud.tammo.master.wrapper;

import java.io.*;

/**
 * Created by Tammo on 27.06.2017.
 */
public class Wrapper {

    private String Adress;
    private int id, cpu;
    private boolean connected;
    private long ram;


    private File file;

    public Wrapper(String Adress, int id) {
        this.Adress = Adress;
        this.id = id;
        this.connected = false;
        this.file = new File("Master//Wrapper", "wrapper-" + id + ".txt");
        this.cpu = 0;
        this.ram = 0;
    }

    public void save(){
        if(!file.getParentFile().exists()){
            file.getParentFile().mkdirs();
        }

        if(!file.exists()){
            try {
                file.createNewFile();
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
                bw.write("Host: " + Adress);
                bw.newLine();
                bw.write("ID: " + id);
                bw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void delete(){
        if(this.file.exists()){
            this.file.delete();
        }
    }

    public String getAdress() {
        return Adress;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
        delete();
        this.file = new File("Cloud//Wrapper", "wrapper-" + id + ".txt");
        save();
    }

    public boolean isConnected(){
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }

    public long getRam() {
        return ram;
    }

    public int getCpu() {
        return cpu;
    }

    public void setCpu(int cpu) {
        this.cpu = cpu;
    }

    public void setRam(long ram) {
        this.ram = ram;
    }
}
