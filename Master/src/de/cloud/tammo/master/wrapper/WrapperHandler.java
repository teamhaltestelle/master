package de.cloud.tammo.master.wrapper;

import de.cloud.tammo.master.logger.Logger;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by Tammo on 27.06.2017.
 */
public class WrapperHandler {

    private ArrayList<Wrapper> wrappers = new ArrayList<>();

    public WrapperHandler() {
        loadWrapper();
    }

    private void loadWrapper(){
        File dir = new File("Master//Wrapper");
        if(!dir.exists()){
            dir.mkdirs();
        }
        for(File wrapperFile : dir.listFiles()){
            try {
                BufferedReader br = new BufferedReader(new FileReader(wrapperFile));
                String host = br.readLine().substring(6);
                int id = Integer.parseInt(br.readLine().substring(4));
                Wrapper w = new Wrapper(host, id);
                this.addWrapper(w);
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public ArrayList<Wrapper> getWrappers() {
        return wrappers;
    }

    public void addWrapper(Wrapper w){
        if(this.wrappers.contains(w)){
            this.wrappers.remove(w);
        }
        this.wrappers.add(w);
    }

    public int getNextID(){
        return wrappers.size() + 1;
    }

    public Wrapper getWrapperByHost(String host){
        for(Wrapper w : getWrappers()){
            if(w.getAdress().equalsIgnoreCase(host)){
                return w;
            }
        }
        return null;
    }

    public Wrapper getWrapperByID(int id){
        for(Wrapper w : getWrappers()){
            if(w.getId() == id){
                return w;
            }
        }
        return null;
    }

    public void searchforWrapper(String host){
        Wrapper w = getWrapperByHost(host);
        if(!w.isConnected()){
            while(!w.isConnected()){
                System.out.flush();
            }
            Logger.addMessage("Der Wrapper-" + w.getId() + " ist nun verbunden!");
        }else{
            Logger.addMessage("Du bist bereits mit dem Wrapper-" + w.getId() + " verbunden!");
        }
    }

    public void removeWrapperbyId(int id){
        Wrapper wrapper = null;
        if(!getWrappers().isEmpty()){
            for(Wrapper w : wrappers){
                if(w.getId() == id){
                    wrapper = w;
                }else if(w.getId() > id){
                    w.setId(w.getId() - 1);
                }
            }
            if(wrapper != null){
                getWrappers().remove(wrapper);
            }
        }else{
            Logger.addMessage("Es ist kein Wrapper vorhanden!");
        }
    }

}
