package de.cloud.tammo.master.command;

import de.cloud.tammo.master.command.executor.GroupCommand;
import de.cloud.tammo.master.command.executor.StopCommand;
import de.cloud.tammo.master.command.executor.UserCommand;
import de.cloud.tammo.master.command.executor.WrapperCommand;

import java.util.ArrayList;

/**
 * Created by Tammo on 26.06.2017.
 */
public class CommandManager {

    private static ArrayList<Command> commands;

    public CommandManager() {
        commands = new ArrayList<>();
        registerCommands();
    }

    private void registerCommands(){
        commands.add(new StopCommand());
        commands.add(new WrapperCommand());
        commands.add(new UserCommand());
        commands.add(new GroupCommand());
    }

    public static ArrayList<Command> getCommands() {
        return commands;
    }
}
