package de.cloud.tammo.master.command.executor;

import de.cloud.tammo.communication.packets.information.StartServerPacket;
import de.cloud.tammo.master.Master;
import de.cloud.tammo.master.command.Command;
import de.cloud.tammo.master.groups.Group;
import de.cloud.tammo.master.groups.GroupType;

/**
 * Created by Tammo on 28.06.2017.
 */
public class GroupCommand extends Command{

    public GroupCommand() {
        super("group", "g");
    }

    public void execute(String[] args) {
        Master.getServer().sendPacket(new StartServerPacket("Skywars-1", 25565, 256, 512, "Skywars"), "127.0.0.1", 226);
        if(args.length == 7){
            if(args[0].equalsIgnoreCase("create")){
                String name = args[1];
                try {
                    int startport = Integer.parseInt(args[2]);
                    try {
                        int anzahl = Integer.parseInt(args[3]);
                        try {
                            int minram = Integer.parseInt(args[4]);
                            try {
                                int maxram = Integer.parseInt(args[5]);
                                GroupType type = null;
                                if(args[6].equalsIgnoreCase("LOBBY")){
                                    type = GroupType.LOBBY;
                                    Group g = new Group(name, startport, anzahl, minram, maxram, type);
                                    Master.getGroupManager().addGroup(g);
                                    display("Du hast erfolgreich die Gruppe " + name + " hinzugefügt!");
                                }else if(args[6].equalsIgnoreCase("DYNAMISCH")){
                                    type = GroupType.DYNAMISCH;
                                    Group g = new Group(name, startport, anzahl, minram, maxram, type);
                                    Master.getGroupManager().addGroup(g);
                                    display("Du hast erfolgreich die Gruppe " + name + " hinzugefügt!");
                                }else{
                                    display("Kein gültiger Typ. Gültig sind: DYNAMISCH, LOBBY");
                                }
                            }catch (NumberFormatException e){
                                display("Der maximale RAM muss eine gültige Zahl sein!");
                            }
                        }catch (NumberFormatException e){
                            display("Der minimale RAM muss eine gültige Zahl sein!");
                        }
                    }catch (NumberFormatException e){
                        display("Die Server Anzahl muss eine gültige Zahl sein!");
                    }
                }catch (NumberFormatException e){
                    display("Der Startport muss eine gültige Zahl sein!");
                }
            }else{
                sendhelp();
            }
        }else if(args.length == 2){
            if(args[0].equalsIgnoreCase("remove")){
                Group group = Master.getGroupManager().getGroupByName(args[1]);
                if(group != null){
                    Master.getGroupManager().removeGroup(group);
                    display("Du hast erfolgreich die Gruppe " + group.getName() + " gelöscht!");
                }else{
                  display("Diese Gruppe existiert nicht!");
                }
            }else{
                sendhelp();
            }
        }else if(args.length == 1){
            if(args[0].equalsIgnoreCase("list")){
                display("");
                display("----Group List----");
                for (Group group : Master.getGroupManager().getGroups()) {
                    display(group.getName());
                }
                display("");
            }else{
                sendhelp();
            }
        }else{
            sendhelp();
        }
    }

    //String name, int startPort, int max, int minRAM, int maxRAM, GroupType type)
    public void sendhelp() {
        display("group create <Name> <StartPort> <Server Anzahl> <min RAM> <max RAM> <Type>");
        display("group remove <Name>");
        display("group list");
    }


}
