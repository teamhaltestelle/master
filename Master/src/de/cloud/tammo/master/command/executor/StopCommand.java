package de.cloud.tammo.master.command.executor;

import de.cloud.tammo.master.command.Command;

/**
 * Created by Tammo on 26.06.2017.
 */
public class StopCommand extends Command{

    public StopCommand() {
        super("stop", "s");
    }

    public void execute(String[] args) {
        System.exit(0);
    }
}
