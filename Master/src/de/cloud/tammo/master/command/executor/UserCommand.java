package de.cloud.tammo.master.command.executor;

import de.cloud.tammo.master.Master;
import de.cloud.tammo.master.command.Command;
import de.cloud.tammo.master.config.User;
import de.cloud.tammo.master.logger.Logger;

/**
 * Created by Tammo on 27.06.2017.
 */
public class UserCommand extends Command{

    public UserCommand() {
        super("user", "u");
    }

    public void execute(String[] args) {
        if(args.length == 3){
            if(args[0].equalsIgnoreCase("add")){
                String name = args[1];
                String password = args[2];
                Master.getUserHandler().addUser(name, password);
                Master.getUserConfig().save();
                Logger.addMessage("Du hast den User " + name + " erfolgreich hinzugefügt!");
            }else{
                sendhelp();
            }
        }else if(args.length == 2){
            if(args[0].equalsIgnoreCase("remove")){
                String name = args[1];
                if(Master.getUserHandler().usernameExist(name)){
                    User u = Master.getUserHandler().getUserByName(name);
                    Master.getUserHandler().removeUser(u);
                }else{
                    Logger.addMessage("Dieser User existiert nicht!");
                }
            }else{
                sendhelp();
            }
        }else if(args.length == 1){
            if(args[0].equalsIgnoreCase("list")){
                Logger.addMessage("----User List----");
                Logger.addMessage("");
                for(User u : Master.getUserHandler().getUsers()){
                    Logger.addMessage(u.getName());
                }
                Logger.addMessage("");
            }else{
                sendhelp();
            }
        }else{
            sendhelp();
        }
    }

    public void sendhelp() {
        display("user add <Name> <Password>");
        display("user remove <Name>");
        display("user list");
    }
}
