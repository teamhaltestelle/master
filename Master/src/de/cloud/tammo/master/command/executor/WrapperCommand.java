package de.cloud.tammo.master.command.executor;

import de.cloud.tammo.master.Master;
import de.cloud.tammo.master.command.Command;
import de.cloud.tammo.master.logger.Logger;
import de.cloud.tammo.master.logger.MessageType;
import de.cloud.tammo.master.wrapper.Wrapper;

/**
 * Created by Tammo on 27.06.2017.
 */
public class WrapperCommand extends Command{

    public WrapperCommand() {
        super("wrapper", "w");
    }

    public void execute(String[] args) {
        if(args.length == 2){
            if(args[0].equalsIgnoreCase("add")){
                Wrapper w = new Wrapper(args[1], Master.getWrapperHandler().getNextID());
                Master.getWrapperHandler().addWrapper(w);
                w.save();
                Logger.addMessage("Du hast den Wrapper-" + w.getId() + " erfolgreich erstellt!");
            }else if(args[0].equalsIgnoreCase("remove")){
                Wrapper w = Master.getWrapperHandler().getWrapperByHost(args[1]);
                if(w != null){
                    w.delete();
                    Master.getWrapperHandler().removeWrapperbyId(w.getId());
                    Logger.addMessage("Du hast den Wrapper-" + w.getId() + " erfolgreich gelöscht!");
                }else{
                    Logger.addMessage("Dieser Wrapper existiert nicht!", MessageType.ERROR);
                }
            }else{
                sendhelp();
            }
        }else if(args.length == 1){
            if(args[0].equalsIgnoreCase("list")){
                Logger.addMessage("---Wrapper List---");
                Logger.addMessage("");
                for(Wrapper w : Master.getWrapperHandler().getWrappers()){
                    if(w.isConnected()){
                        Logger.addMessage("Wrapper-" + w.getId() + " ist Online auf der IP: " + w.getAdress());
                    }else{
                        Logger.addMessage("Wrapper-" + w.getId() + " ist Offline");
                    }
                }
                Logger.addMessage("");
            }else{
                sendhelp();
            }
        }else{
            sendhelp();
        }
    }

    public void sendhelp() {
        display("Wrapper add <IP>");
        display("Wrapper remove <IP>");
        display("Wrapper list");
    }
}
