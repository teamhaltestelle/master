package de.cloud.tammo.master.command;

import de.cloud.tammo.master.logger.Logger;

/**
 * Created by Tammo on 26.06.2017.
 */
public class Command {

    private String command;
    private String[] aliases;

    public Command(String command) {
        this.command = command;
        this.aliases = new String[]{};
    }

    public Command(String command, String... aliases) {
        this.command = command;
        this.aliases = aliases;
    }

    public void execute(String[] args){}

    public void sendhelp(){}

    public String[] getAliases() {
        return aliases;
    }

    public String getCommand() {
        return command;
    }

    public void display(String msg){
        Logger.addMessage(msg);
    }

}
