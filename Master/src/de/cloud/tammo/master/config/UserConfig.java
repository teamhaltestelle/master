package de.cloud.tammo.master.config;

import de.cloud.tammo.master.Master;

import java.io.*;

/**
 * Created by Tammo on 27.06.2017.
 */
public class UserConfig {

    private File cfg;

    public UserConfig() {
        cfg = new File("Master", "User.txt");
        checkFile();
    }

    private void checkFile(){
        if(!cfg.getParentFile().exists()){
            cfg.getParentFile().mkdirs();
        }
        if(!cfg.exists()){
            try {
                cfg.createNewFile();
                Master.getUserHandler().addUser("Tammo", "Test");
            }catch (IOException e) {
                e.printStackTrace();
            }
        }
        loadUser();
    }

    public void save(){
        try{
            BufferedWriter bw = new BufferedWriter(new FileWriter(cfg));
            for(User u : Master.getUserHandler().getUsers()){
                bw.write(u.getName() + ":" + u.getPassword());
                bw.newLine();
            }
            bw.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    private void loadUser(){
        try {
            BufferedReader br = new BufferedReader(new FileReader(cfg));
            String line;
            while((line = br.readLine()) != null){
                String[] arg = line.split(":");
                String name = arg[0];
                String passwort = arg[1];
                Master.getUserHandler().addUser(name, passwort);
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        save();
    }
}
