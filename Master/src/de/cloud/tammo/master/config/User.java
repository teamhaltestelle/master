package de.cloud.tammo.master.config;

/**
 * Created by Tammo on 27.06.2017.
 */
public class User {

    private String name, password;

    public User (String name, String password){
        this.name = name;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }
}
