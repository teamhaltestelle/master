package de.cloud.tammo.master.config;

import de.cloud.tammo.master.Master;
import de.cloud.tammo.master.logger.Logger;
import de.cloud.tammo.master.logger.MessageType;

import java.util.ArrayList;

/**
 * Created by Tammo on 27.06.2017.
 */
public class UserHandler {

    private ArrayList<User> users;
    private boolean logggedIn;

    public UserHandler(){
        users = new ArrayList<>();
        logggedIn = false;
    }

    public ArrayList<User> getUsers() {
        return users;
    }

    public boolean isLogggedIn() {
        return logggedIn;
    }

    public boolean checkAuth(String name, String password){
        for(User u : getUsers()){
            if(u.getName().equalsIgnoreCase(name)){
                if(u.getPassword().equalsIgnoreCase(password)){
                    Logger.addMessage("Du hast dich erfolgreich eingeloggt!");
                    logggedIn = true;
                    return true;
                }else{
                    Logger.addMessage("Das Passwort wurde falsch eingegeben!", MessageType.ERROR);
                    return false;
                }
            }
        }
        return false;
    }

    public boolean usernameExist(String name){
        for(User u : getUsers()){
            if(u.getName().equalsIgnoreCase(name)){
                return true;
            }
        }
        return false;
    }

    public User getUserByName(String name){
        for(User u : users){
            if(u.getName().equalsIgnoreCase(name)){
                return u;
            }
        }
        return null;
    }

    public void removeUser(User u){
        if(users.size() >= 1){
            if(users.contains(u)){
                users.remove(u);
                Master.getUserConfig().save();
                Logger.addMessage("Du hast den User " + u.getName() + " erfolgreich gelöscht!");
            }
        }else{
            Logger.addMessage("Du kannst diesen User nicht löschen, bevor ein neuer erstellt wurde!", MessageType.ERROR);
        }
    }

    public void addUser(String name, String password){
        User user = new User(name, password);
        users.add(user);
    }
}
