package de.cloud.tammo.master.groups;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by Tammo on 27.06.2017.
 */
public class Group {

    private String name;
    private int startPort, max, minRAM, maxRAM, amount;
    private GroupType type;
    private File cfg;
    private ArrayList<Integer> ports;

    public Group(String name, int startPort, int max, int minRAM, int maxRAM, GroupType type) {
        this.name = name;
        this.startPort = startPort;
        this.max = max;
        this.minRAM = minRAM;
        this.maxRAM = maxRAM;
        this.amount = 0;
        this.type = type;
        this.ports = new ArrayList<>();
        this.cfg = new File("Master//Groups//", name + ".txt");
        checkFile();
    }

    private void checkFile(){
        if(!cfg.getParentFile().exists()){
            cfg.getParentFile().mkdirs();
        }
        if(!cfg.exists()){
            try {
                cfg.createNewFile();
                BufferedWriter bw = new BufferedWriter(new FileWriter(this.cfg));
                bw.write("Startport: " + this.startPort);
                bw.newLine();
                bw.write("Max. Server: " + this.max);
                bw.newLine();
                bw.write("Max. Ram: " + maxRAM);
                bw.newLine();
                bw.write("Min. Ram: " + minRAM);
                bw.newLine();
                bw.write("Type: " + type.toString().toUpperCase());
                bw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void delete(){
        if(this.cfg.exists()){
            this.cfg.delete();
        }
    }

    public String getName() {
        return name;
    }

    public int getMax() {
        return max;
    }

    public int getMaxRAM() {
        return maxRAM;
    }

    public int getMinRAM() {
        return minRAM;
    }

    public int getStartPort() {
        return startPort;
    }

    public GroupType getType() {
        return type;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getNextID(){
        //TODO
        return ports.size() + 1;
    }

    public int getNextPort(){

        return 0;
    }

    public void removeServer(int port){

    }
}
