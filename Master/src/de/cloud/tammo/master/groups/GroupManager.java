package de.cloud.tammo.master.groups;

import de.cloud.tammo.master.logger.Logger;
import de.cloud.tammo.master.logger.MessageType;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by Tammo on 27.06.2017.
 */

public class GroupManager {

    private ArrayList<Group> groups;

    public GroupManager() {
        this.groups = new ArrayList<>();
        loadGroups();
    }

    private void loadGroups(){
        File dir = new File("Master//Groups");
        if(dir.exists()){
            for(File child : dir.listFiles()){
                try {
                    String name = child.getName().replace(".txt", "");
                    BufferedReader br = new BufferedReader(new FileReader(child));
                    int startport = Integer.parseInt(br.readLine().replace("Startport: ", ""));
                    int max = Integer.parseInt(br.readLine().replace("Max. Server: ", ""));
                    int maxRAM = Integer.parseInt(br.readLine().replace("Max. Ram: ", ""));
                    int minRam = Integer.parseInt(br.readLine().replace("Min. Ram: ", ""));
                    String type = br.readLine().replace("Type: ", "");
                    br.close();
                    Group group = new Group(name, startport, max, minRam, maxRAM, GroupType.valueOf(type));
                    addGroup(group);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }else{
            dir.mkdirs();
        }
    }

    public void addGroup(Group group){
        groups.add(group);
    }

    public void removeGroup(Group g){
        if(groups.contains(g)){
            groups.remove(g);
            g.delete();
        }else{
            Logger.addMessage("Diese Gruppe existiert nicht!", MessageType.ERROR);
        }
    }

    public Group getGroupByName(String name){
        for(Group g : getGroups()){
            if(g.getName().equalsIgnoreCase(name)){
                return g;
            }
        }
        return null;
    }

    public ArrayList<Group> getGroups() {
        return groups;
    }
}
